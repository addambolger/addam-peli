var config = {

    deps: [
        "js/custom"
    ],
    paths: {
        slick: "js/slick"
    },
    shim: {
        slick: {
            deps: ['jquery']
        }
    }

};