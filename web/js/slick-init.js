define(["jquery", "slick"],

    function ($, slick) {

        'use strict';

        return function (config, element) {
            $(element).slick(config);

            $(window).resize(function () {
                if (!$(element).hasClass('slick-initialized')) {
                    $(element).slick(config);
                }
                else {
                    $(element)[0].slick.refresh();
                }
            });
        }

    });
